"""cars_ads URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from ads.views import HomeView, ajax_get_models, ajax_get_submodels, ajax_get_modifies, ajax_new_inquiry, add_new_inquiry, ajax_get_table_inq,\
    ajax_check_favorite, InqView, login, login_page, logout_view

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='HomeView'),

    url(r'^add_inquiry/$', add_new_inquiry),

    #only ajax url
    url(r'^models/$', ajax_get_models, name='models'),
    url(r'^submodels/$', ajax_get_submodels, name='submodels'),
    url(r'^modifies/$', ajax_get_modifies, name='modifies'),
    url(r'^post_new_inquiry/$', ajax_new_inquiry, name='post_inquiry'),
    url(r'^inq/$', ajax_get_table_inq),
    url(r'^get_inq/(?P<pk>\d+)/$$', InqView.as_view(), name='InqView'),
    url(r'^check_favor/(?P<pk>\d+)/$$', ajax_check_favorite),


    #логин
    url(r'^accounts/login_page/$', login_page),
    url(r'^accounts/login/$', login, name='login'),
    url(r'^accounts/logout/$', logout_view, name='logout'),
    url(r'^admin/', admin.site.urls),

]
