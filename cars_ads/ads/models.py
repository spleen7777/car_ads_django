import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import datetime


# Create your models here.

class Brands(models.Model):
    name = models.CharField(max_length=100, unique=True, help_text='Название марки')
    hide = models.BooleanField(default=False)
    sync_id = models.IntegerField(unique=True, null=True)

    class Meta:
        verbose_name = "Марка"
        verbose_name_plural = "Марки"

    def __str__(self):
        return self.name


class Models(models.Model):
    name = models.CharField(max_length=128, help_text='Название модели')
    brand = models.ForeignKey(Brands, on_delete=models.CASCADE)
    sync_id = models.IntegerField(unique=True, null=True)

    class Meta:
        verbose_name = "Модельный ряд"
        verbose_name_plural = "Модельные ряды"

    def __str__(self):
        return self.name


class SubModels(models.Model):
    name = models.CharField(max_length=128, help_text='Подмодель')
    brand = models.ForeignKey(Brands, on_delete=models.CASCADE)
    model = models.ForeignKey(Models, on_delete=models.CASCADE)
    sync_id = models.IntegerField(unique=True, null=True)

    class Meta:
        verbose_name = "Модель"
        verbose_name_plural = "Модели"

    def __str__(self):
        return self.name


class Modify(models.Model):
    ENGINE_TYPES = (
        ('бензин', 'бензин'),
        ('газ', 'газ'),
        ('гибрид', 'гибрид'),
        ('двухтактный', 'двухтактный'),
        ('дизель', 'дизель'),
        ('ротор', 'ротор'),
        ('электро', 'электро'),
    )

    DRIVE_TYPES = (
        ('полный', 'полный'),
        ('задний', 'задний'),
        ('передний', 'передний'),
    )

    name = models.CharField(max_length=128, help_text='Модификации')
    brand = models.ForeignKey(Brands, on_delete=models.CASCADE)
    model = models.ForeignKey(Models, on_delete=models.CASCADE)
    submodel = models.ForeignKey(SubModels, on_delete=models.CASCADE, blank=True, null=True)
    enginetype = models.CharField(max_length=30, choices=ENGINE_TYPES, help_text='тип двигателя', blank=True, null=True)
    drivetype = models.CharField(max_length=20, choices=DRIVE_TYPES, help_text='привод', blank=True, null=True)
    release_from = models.IntegerField(help_text='Год начала выпуска', blank=True, null=True)
    release_to = models.IntegerField(help_text='Год окончания выпуска', blank=True, null=True)
    sync_id = models.IntegerField(unique=True, null=True)

    class Meta:
        verbose_name = "Модификация"
        verbose_name_plural = "Модификации"

    def __str__(self):
        return self.name + ' ' + str(self.release_from) + '-' + str(self.release_to)


class Regions(models.Model):
    name = models.CharField(max_length=128, help_text='Регион')

    class Meta:
        verbose_name = "Регион"
        verbose_name_plural = "Регионы"

    def __str__(self):
        return self.name


class Inquiry(models.Model):
    PARTS = (
        ('Двигатель', 'Двигатель'),
        ('КПП', 'КПП'),
    )

    date = models.DateTimeField(auto_now=datetime.date.today())
    brand = models.ForeignKey(Brands, on_delete=models.CASCADE)
    model = models.ForeignKey(Models, on_delete=models.CASCADE)
    submodel = models.ForeignKey(SubModels, on_delete=models.CASCADE, blank=True, null=True)
    modify = models.ForeignKey(Modify, on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField(max_length=300, blank=True)
    customer_name = models.CharField(max_length=50)
    region = models.ForeignKey(Regions, blank=True)
    city = models.CharField(max_length=25)
    tel = models.IntegerField(blank=True)
    email = models.CharField(max_length=30, blank=True)
    part_type = models.CharField(max_length=20, blank=False, choices=PARTS)
    part_code = models.CharField(max_length=20, blank=True)
    hide = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Запрос"
        verbose_name_plural = "Запросы"

    def __str__(self):
        return str(self.part_type) + ' - ' + str(self.brand)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='user')
    phone = models.CharField(max_length=20, blank=True, default='', help_text='Телефон')
    city = models.CharField(max_length=100, default='', blank=True, help_text='Город')
    region = models.ForeignKey(Regions, blank=True, null=True)
    organization = models.CharField(max_length=100, default='', blank=True, help_text='Организация')

    class Meta:
        verbose_name = "Профиль"
        verbose_name_plural = "Профили"

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name


def create_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_profile = UserProfile(user=user)
        user_profile.save()


post_save.connect(create_profile, sender=User)


class ReadInq(models.Model):
    inquiry = models.ForeignKey(Inquiry, blank=False)
    profile = models.ForeignKey(UserProfile, blank=False)
    date = models.DateTimeField(default=datetime.date.today())

    class Meta:
        verbose_name = "Просмотренное объявления"
        verbose_name_plural = "Просмотренные объявления"

    def __str__(self):
        return str(self.profile) + ' - ' + str(self.profile)


class FavoriteInq(models.Model):
    inquiry = models.ForeignKey(Inquiry, blank=False)
    profile = models.ForeignKey(UserProfile, blank=False)
    date = models.DateTimeField(default=datetime.date.today())

    class Meta:
        verbose_name = "Избранное"
        verbose_name_plural = "Избранные"
