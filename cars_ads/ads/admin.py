from django.contrib import admin

# Register your models here.
from django.contrib import admin
from ads.models import Brands, Regions, Inquiry, UserProfile


class BrandsAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    search_fields = ['id', 'name', 'hide']
    list_display = ('name', 'hide')
    list_filter = ('name', 'hide')
    fields = ('id', 'name', 'hide')
    list_editable = ('hide',)

    class Meta:
        model = Brands

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
admin.site.register(Brands, BrandsAdmin )


class RegionAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    search_fields = ['id', 'name']
    list_display = ('name',)
    list_filter = ('name',)
    fields = ('id', 'name',)

    class Meta:
        model = Regions

admin.site.register(Regions, RegionAdmin)

class InquiryAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    search_fields = ['region']
    list_display = ( 'date' , 'brand' , 'model' , 'submodel' , 'modify' , 'description' , 'customer_name' , 'region' , 'city' , 'tel' , 'email' , 'part_type' , 'part_code' , 'hide',)
    list_filter = ('brand', 'region', 'model',)
    fields = ( 'brand' , 'model' , 'submodel' , 'modify' , 'description' , 'customer_name' , 'region' , 'city' , 'tel' , 'email' , 'part_type' , 'part_code' , 'hide',)

    class Meta:
        model = Inquiry

admin.site.register(Inquiry, InquiryAdmin)

class UserProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    search_fields = ['user', 'phone', 'city', 'region']
    list_display = ( 'user', 'phone', 'city', 'region',)
    list_filter = ('user', 'region', )
    fields = ( 'user', 'phone', 'city', 'region', 'organization',)

admin.site.register(UserProfile, UserProfileAdmin)

