# -*- coding: utf-8 -*-
__author__ = 'Freeman'

import os, sqlite3

from ads.models import Brands, Models, SubModels, Modify, Regions

PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/'
DB_NAME = 'cars_base.db'

class ExtDB():

    def __init__(self):
        self.conn = sqlite3.connect(PATH + DB_NAME)

    def query_with_commit(self, query):
        self.error = None
        self.make_query(query)
        try:
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
        finally:
            self.conn.close()

    def make_query(self, query):
        self.cursor = self.conn.execute(query)


class UpdateCatalogs():

    def __init__(self):
        self.extdb = ExtDB()

    def updatebrands(self):
        q = 'SELECT NAME, ID FROM auto_brands'
        self.extdb.make_query(q)
        for row in self.extdb.cursor:
            name = row[0]
            sync_id = row[1]
            brand_obj = Brands.objects.filter(sync_id=sync_id)

            if not len(brand_obj):
                brand = Brands(name=name, sync_id=sync_id)
                brand.save()

    def updatemodels(self):
        q = 'SELECT NAME, ID, BRAND_ID FROM auto_models'
        self.extdb.make_query(q)
        for row in self.extdb.cursor:
            name = row[0]
            sync_id = row[1]
            brand_id = row[2]
            brand_obj = Brands.objects.filter(sync_id=brand_id).get()

            model_obj = Models.objects.filter(sync_id=sync_id)
            if not len(model_obj):
                model = Models(name=name, sync_id=sync_id, brand=brand_obj)
                model.save()

    def updatesubmodels(self):
        q = 'SELECT NAME, ID, MODEL_ID, BRAND_ID FROM auto_submodels'
        self.extdb.make_query(q)
        for row in self.extdb.cursor:
            name = row[0]
            sync_id = row[1]
            brand_id = row[3]
            model_id = row[2]
            brand_obj = Brands.objects.filter(sync_id=brand_id).get()
            model_obj = Models.objects.filter(sync_id=model_id).get()

            submodel_obj = SubModels.objects.filter(sync_id=sync_id)
            if not len(submodel_obj):
                submodel = SubModels(name=name, sync_id=sync_id, brand=brand_obj, model=model_obj)
                submodel.save()

    def updatemodify(self):
        Modify.objects.all().delete()
        q = 'SELECT NAME, ID, BRAND_ID, MODEL_ID, SUBMODEL_ID, ENGINE_TYPE, DRIVE_TYPE, RELEASE_FROM, RELEASE_TO FROM auto_modify'
        self.extdb.make_query(q)
        for row in self.extdb.cursor:
            name = row[0]
            sync_id = row[1]
            brand_id = row[2]
            model_id = row[3]
            submodel_id = row[4]
            enginetype = row[5]
            drivetype = row[6]
            release_from = row[7]
            release_to = row[8]

            brand_obj = Brands.objects.filter(sync_id=brand_id).get()
            model_obj = Models.objects.filter(sync_id=model_id).get()

            if submodel_id:
                try:
                    submodel_obj = SubModels.objects.filter(sync_id=submodel_id).get()
                except Exception as e:
                    print(e)
                    break
            else:
                submodel_obj = None

            if release_from == 0:
                release_from = None

            if release_to == 0:
                release_to = None

            modif_obj = Modify.objects.filter(sync_id=sync_id)
            if not len(modif_obj):
                modif = Modify(name=name, sync_id=sync_id, brand=brand_obj,
                               model=model_obj, submodel=submodel_obj, enginetype=enginetype,
                               drivetype=drivetype, release_from=release_from, release_to=release_to)
                try:
                    modif.save()
                except Exception as e:
                    print(e)
                    break

    def update_Regions(self):
        region_list = [
            "респ. Адыгея",
            "респ. Алтай",
            "респ. Башкортостан",
            "респ. Бурятия",
            "респ. Дагестан",
            "респ. Ингушетия",
            "респ. Кабардино-Балкария",
            "респ. Калмыкия",
            "респ. Карачаево-Черкесия",
            "респ. Карелия",
            "респ. Коми",
            "респ. Крым",
            "респ. Марий Эл",
            "респ. Мордовия",
            "респ. Саха (Якутия)",
            "респ. Северная Осетия — Алания",
            "респ. Татарстан",
            "респ. Тыва",
            "респ. Удмуртия",
            "респ. Хакасия",
            "респ. Чечня",
            "респ. Чувашия",
            "Алтайский край",
            "Забайкальский край",
            "Камчатский край",
            "Краснодарский край",
            "Красноярский край",
            "Пермский край",
            "Приморский край",
            "Ставропольский край",
            "Хабаровский край",
            "Амурская обл.",
            "Архангельская обл.",
            "Астраханская обл.",
            "Белгородская обл.",
            "Брянская обл.",
            "Владимирская обл.",
            "Волгоградская обл.",
            "Вологодская обл.",
            "Воронежская обл.",
            "Ивановская обл.",
            "Иркутская обл.",
            "Калининградская обл.",
            "Калужская обл.",
            "Кемеровская обл.",
            "Кировская обл.",
            "Костромская обл.",
            "Курганская обл.",
            "Курская обл.",
            "Ленинградская обл.",
            "Липецкая обл.",
            "Магаданская обл.",
            "Московская обл.",
            "Мурманская обл.",
            "Нижегородская обл.",
            "Новгородская обл.",
            "Новосибирская обл.",
            "Омская обл.",
            "Оренбургская обл.",
            "Орловская обл.",
            "Пензенская обл.",
            "Псковская обл.",
            "Ростовская обл.",
            "Рязанская обл.",
            "Самарская обл.",
            "Саратовская обл.",
            "Сахалинская обл.",
            "Свердловская обл.",
            "Смоленская обл.",
            "Тамбовская обл.",
            "Тверская обл.",
            "Томская обл.",
            "Тульская обл.",
            "Тюменская обл.",
            "Ульяновская обл.",
            "Челябинская обл.",
            "Ярославская обл.",
            "Москва",
            "Санкт-Петербург",
            "Севастополь",
            "Еврейская АО",
            "Ненецкий АО",
            "Ханты-Мансийский АО — Югра",
            "Чукотский АО",
            "Ямало-Ненецкий АО"
        ]

        for name in region_list:
            region_obj = Regions.objects.filter(name=name)
            if not region_obj:
                region = Regions(name=name)
                region.save()



