
from ads.models import Brands, Regions, Inquiry, ReadInq, FavoriteInq

def get_inquires_by_filter(brand_list, region_list, part_type, favinq_ids):

    filters = {'hide':False}

    if favinq_ids:
        filters.update({'id__in':favinq_ids})

    if len(brand_list) > 0:
        brands_select = Brands.objects.filter(name__in=brand_list)
        filters.update({'brand':brands_select})

    if len(region_list) > 0:
        region_select = Regions.objects.filter(name__in=region_list)
        filters.update({'region':region_select})

    if part_type == 'Двигатель' or part_type == 'КПП':
        filters.update({'part_type':part_type})

    inquires = Inquiry.objects.filter(**filters).order_by('-date')

    return inquires

def get_favorit_inq(userprofile):
    favinq = FavoriteInq.objects.filter(profile=userprofile)
    return favinq

#запишим просмотренные объявления клиентом
# для того чтобы их подсветить
def addReadInq(profile, inquiry):
    try:
       readinq = ReadInq(profile=profile, inquiry=inquiry)
       readinq.save()
    except Exception as er:
        print(er)

#записать объявление или удаляет заявку из избранного
def checkFavoriteInq(currentFav):
    try:
        isfavor = FavoriteInq.objects.filter(profile=currentFav.profile, inquiry=currentFav.inquires)
        if not isfavor:
            addFavoriteInq(currentFav)
            currentFav.action = 'add'
        else:
            delFavoriteInq(isfavor)
            currentFav.action = 'del'
    except Exception as er:
        print(er)

def addFavoriteInq(currentFav):
    try:
        favorinq = FavoriteInq(profile=currentFav.profile, inquiry=currentFav.inquires)
        favorinq.save()
    except Exception as er:
        print(er)

#удалить объявление из избранного
def delFavoriteInq(favorites):
    try:
        favorites.delete()
    except Exception as er:
        print(er)







