import json, datetime
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.db import connection
from django.template.loader import render_to_string

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib import auth
from django.contrib.auth.models import User, Group
from django.views.generic.base import View
from ads.models import Brands, Models, SubModels, Modify, Regions, Inquiry, ReadInq, FavoriteInq
import ads.helper_view as helper
#import ads.update_auto_catalogs as Uploud



class HomeView(View):
    def get(self, request, *args, **kwargs):
        result = {}
        tmpl = 'home.html'
        brand_for_selector = Brands.objects.filter(hide=False)
        regions_for_selector = Regions.objects.all()

        # заглушка для корректной компиляции шаблона
        inquires = {}
        paging = {'page':1, 'num_pages': [1] }

        result = {'inquires': inquires, 'paging':paging, 'brands': brand_for_selector, 'regions':regions_for_selector}
        return render(request, tmpl, result)


## Popup окно с детальной информацией об одной заявке
class InqView(View):
    def get(self, request, *args, **kwargs):
        pk = kwargs['pk']
        if request.user.is_authenticated():
            if pk:
                inq = Inquiry.objects.get(pk=pk)

                html = render_to_string(template_name='inquiry_table/inquiry_popup.html', context={'inquiry': inq, 'user': request.user})
                profile = request.user.user
                helper.addReadInq(profile, inq)
            else:
                html = '<h1>Нет данных</h1>'
        else:
            html = render_to_string(template_name='inquiry_table/inq_notify.html')

        return HttpResponse(html)


# Добавление объявлений
#
@login_required()
def add_new_inquiry(request):
    tmpl = 'add_inquiry.html'
    brands = Brands.objects.filter(hide=False)
    regions = Regions.objects.all()
    date = datetime.date.today()
    result = {'brands': brands, 'regions': regions, 'date': date}
    return render(request, tmpl, result)


#
#   AJAX add_inquery
#

def ajax_get_table_inq(request, *args, **kwargs):
    tmpl_table = 'inquiry_table/inquiry_table_data.html'
    tmpl_paginator = 'inquiry_table/inquiry_table_pag.html'

    # проверим есть ли отбор по избранному в парраметрах
    favinq_ids = set()
    if request.user.is_authenticated():
        if request.GET.get('favorit') == 'true':
            favinq = helper.get_favorit_inq(request.user.user)
            favinq_ids = set(x.inquiry_id for x in favinq)


    #список заявок по фильтрам
    inquires = helper.get_inquires_by_filter(request.GET.getlist('brands[]'),
                                             request.GET.getlist('regions[]'),
                                             request.GET.get('part_type'), favinq_ids)
   # inquires = Inquiry.objects.filter(hide=False).order_by('-date')

    paginator = Paginator(inquires, 10)  # Show 5  per page
    page = request.GET.get('page')
    try:
        inquires = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        inquires = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        inquires = paginator.page(paginator.num_pages)

    paging = {'page': inquires.number, 'num_pages': [x for x in paginator.page_range[
                                                                max(inquires.number - 6, 0): max(inquires.number + 5,
                                                                                                     5)]]}  # отображаем только ссылок в пагинаторе
    #прочитанные объявления. для их пометки как прочитанные
    # избранные объявления
    if request.user.is_authenticated():
        profile = request.user.user
        favorInq = FavoriteInq.objects.filter(profile=profile, inquiry__in=inquires.object_list)
        readInq = ReadInq.objects.filter(profile=profile, inquiry__in=inquires.object_list)
    else:
        favorInq = []
        readInq = []

    result = {'inquires': inquires, 'paging': paging, 'user': request.user, 'readInq': set(x.inquiry for x in readInq), 'favorInq': set(x.inquiry for x in favorInq)}

    inq_table = render_to_string(template_name=tmpl_table, context=result)
    inq_paginator = render_to_string(template_name=tmpl_paginator, context=result)
    req = json.dumps({'inq_table': inq_table, 'inq_paginator': inq_paginator})
    return HttpResponse(req)


@login_required()
def ajax_get_models(request):
    tmpl = 'models_ajax.html'
    if request.method == "GET":
        brand_id = request.GET['brand_id']
        try:
            models = Models.objects.filter(brand=brand_id)
        except:
            models = {}

        context = {'models': models}

        html_text = render_to_string(template_name=tmpl, context=context)

        return HttpResponse(json.dumps({'html': html_text}))


@login_required()
def ajax_get_submodels(request):
    tmpl = 'submodels_ajax.html'
    if request.method == "GET":
        model_id = request.GET['model_id']
        try:
            submodels = SubModels.objects.filter(model=model_id)
        except:
            submodels = {}

        context = {'submodels': submodels}

        html_text = render_to_string(template_name=tmpl, context=context)

        return HttpResponse(json.dumps({'html': html_text}))


@login_required()
def ajax_get_modifies(request):
    tmpl = 'modify_ajax.html'
    if request.method == "GET":
        model_id = request.GET['model_id']
        submodel_id = request.GET['submodel_id']
        try:
            if (submodel_id == '0'):
                modifies = Modify.objects.filter(model=model_id).order_by('-release_from')
            else:
                modifies = Modify.objects.filter(submodel=submodel_id).order_by('-release_from')
        except:
            modifies = {}

        context = {'modifies': modifies}

        html_text = render_to_string(template_name=tmpl, context=context)

        return HttpResponse(json.dumps({'html': html_text}))

@login_required()
def ajax_new_inquiry(request):
    if request.method == "POST":
        brand_id = request.POST['brand_id']
        model_id = request.POST['model_id']
        submodel_id = request.POST['submodel_id']
        modify_id = request.POST['modify_id']
        part_code = request.POST['part_code']
        part_type = request.POST['part_type']
        region_name = request.POST['region']
        customer_name = request.POST['customer_name']
        city = request.POST['city']
        tel = request.POST['tel']
        desc = request.POST['desc']
        email = request.POST['email']
        valid = request.POST['valid']

        message = ''
        saving = True

        try:
            brand = Brands.objects.get(pk=brand_id)
        except Exception as e:
            message = message +str(e) + '\n'
            brand = None

        try:
            model = Models.objects.get(pk=model_id)
        except Exception as e:
            message = message +str(e) + '\n'
            model = None

        try:
            submodel = SubModels.objects.get(pk=submodel_id)
        except Exception as e:
            message = message +str(e) + '\n'
            submodel =None

        try:
            modify = Modify.objects.get(pk=modify_id)
        except Exception as e:
            message = message +str(e) + '\n'
            modify = None

        try:
            region = Regions.objects.filter(name=region_name)[0]
        except Exception as e:
            message = message +str(e) + '\n'
            region = None

        try:

            inquiry = Inquiry(date=datetime.date.today(), brand=brand, model=model, submodel=submodel, modify=modify, description=desc,
                              customer_name=customer_name, region=region, city=city, tel=tel, email=email, part_type=part_type, part_code=part_code, hide=False)
            inquiry.save()
        except Exception as e:
            message = message +str(e) + '\n'
            saving = False

        return HttpResponse(json.dumps({'valid':saving, 'message': message}))

@login_required()
def ajax_check_favorite(request, *args, **kwargs):
    pk = kwargs['pk']
    if request.user.is_authenticated():
        class CurrentFav:
            profile = request.user.user
            inquires = Inquiry.objects.get(pk=pk)
            action = ''
        helper.checkFavoriteInq(CurrentFav)

        return HttpResponse(json.dumps({'action': CurrentFav.action}))

#
# LOGIN - LOGOUT
#

def login(request):
    if "cancel" in request.POST:
        return  render(request, 'login/login_page.html')
    else:
        if not request.POST.get('remember_me', None):
            request.session.set_expiry(0)

        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # Правильный пароль и пользователь "активен"
            auth.login(request, user)
            # Перенаправление на "правильную" страницу
            return HttpResponseRedirect('/')
        else:
            result = {'error': True, 'message':'Не верно введен логин или пароль'}
            # Отображение страницы с ошибкой
            return  render(request, 'login/login_page.html', result)


def login_page(request):
    result = {'error': False, 'message':''}
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        return render(request, 'login/login_page.html')


def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect('/')
