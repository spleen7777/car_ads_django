$(document).ready(function () {

    var formData = {};
    var csrftoken = getCookie('csrftoken');

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
}

    //////////////////////
    ///    AJAX запросы и обновления формы /////
    //////////////////////
    // при изменении марки
    $('body').on('change', '#brands', function () {
        getModels();
    });

    // при изменении модели
    $('body').on('change', '#models', function () {
        getSubModels();
    });

    // при изменении под модели
     $('body').on('change', '#submodels', function () {
        getModifies();
    });

    //при клике на поле модификации
    $('body').on('click', '.mod', function () {
        checkedModify(this)
    });

    // устанавливает галочки только на одну строку таблицы
    function checkedModify(elem){
        var trs = $('.mod');
        for (var i = 0; i < trs.length; i++) {
            var currentTr = trs[i];
            if (currentTr == elem){
                currentTr.cells[0].children[0].checked = true;
                $('#'+currentTr.id).addClass('activemodify')
            }
            else{
                currentTr.cells[0].children[0].checked = false;
                $('#'+currentTr.id).attr('class', 'mod')

            }
        }

    }

    // возвращает текущее значение выбранной модификации

    // возвращает текущее зачение Марки авто
    function getcurrentBrandsid() {
        var currentOption = $('#brands').val();
        var selectListBrands = $('#brands')[0];
        if (currentOption != '') {

            for (var i = 0; i < selectListBrands.length; i++) {
                var option = selectListBrands[i].value;
                if (option.toUpperCase() == currentOption.toUpperCase()) {
                    return selectListBrands[i].id;
                }
            }
        }

        return 0
    }

    // возвращает текущее значенние Модели авто
    function getcurrentModelsid() {
        var currentOption = $('#models').val();
        var selectListModels = $('#models')[0];
        if (currentOption != '') {

            for (var i = 0; i < selectListModels.length; i++) {
                var option = selectListModels[i].value;
                if (option.toUpperCase() == currentOption.toUpperCase()) {
                    return selectListModels[i].id;
                }
            }
        }
        return 0
    }

    // возвращает текущее значение Под модели авто
    function getcurrentSubModelsId() {
        var currentOption = $('#submodels').val();
        var selectListSubModels = $('#submodels')[0];
        if (currentOption != '') {

            for (var i = 0; i < selectListSubModels.length; i++) {
                var option = selectListSubModels[i].value;
                if (option.toUpperCase() == currentOption.toUpperCase()) {
                    return selectListSubModels[i].id;
                }
            }
        }
        return 0
    }

    // возвращает значение текущей модефикации
    function getcurrentModifyId(){
        var currentModifyid = $('.activemodify');
        if (currentModifyid.length == 0){
            return 0;
        }
        else{
            return currentModifyid[0].id
        }
    }

    function getModels() {
        var brand_id = getcurrentBrandsid();
        var modelsElem = $('#models_row');
        if (brand_id == 0){
            insertHTML(modelsElem, '')
        }

        $.ajax({
            type: 'GET',
            url: '/models/',
            data: {
                brand_id: brand_id
            },
            success: function (data) {
                var req = JSON.parse(data);
                insertHTML(modelsElem, req['html']);

                getSubModels();
            },
            fail: function (data) {
                insertHTML(modelsElem, '')
            }

        });
    }

    function getSubModels() {
        var model_id = getcurrentModelsid();
        var submodelsElem = $('#submodels_row');

        if (model_id == 0){
            insertHTML(submodelsElem, '')
        }

        $.ajax({
            type: 'GET',
            url: '/submodels/',
            data: {
                model_id: model_id
            },
            success: function (data) {
                var req = JSON.parse(data);
                insertHTML(submodelsElem, req['html']);

                getModifies();
            },
            fail: function (data) {
                insertHTML(submodelsElem, '')
            }

        });
    }

    function getModifies(){
        var submodel_id = getcurrentSubModelsId();
        var model_id = getcurrentModelsid();
        var modifiesElem = $('#modifies_row');

        if (model_id == 0){
            insertHTML(modifiesElem, '')
        }

        $.ajax({
            type: 'GET',
            url: '/modifies/',
            data: {
                submodel_id: submodel_id,
                model_id: model_id
            },
            success: function (data) {
                var req = JSON.parse(data);
                insertHTML(modifiesElem, req['html'])
            },
            fail: function (data) {
                insertHTML(modifiesElem, '')
            }

        });
    }

    function insertHTML(elem, html) {
        elem.html(html)
    }

    //////////////////////
    ///   Сохранение и валидация формы /////
    //////////////////////

     $('body').on('click', '#submit', function () {
        saveForm();
    });

    function postForm(){
        if (formData.valid == true){
        $.ajax({
                 type:"POST",
                 url:"/post_new_inquiry/",
                 data: formData,
                 success: function(data){
                     req = JSON.parse(data);
                     reqestPost(req);
                    },
                 fail:
                     reqestPost(
                         {'valid': false, 'message': 'Ошибка передачи данных на сервер'})

            });


        }
    }

    function reqestPost(req){
        if (req['valid'] == true){
            $("#success").html("<div class='callout callout-success'>" +
                " <h3>Объект успешно добавлен в базу</h3><p> Info</p><p>"+ req['message'] +" </p></div>");
            $('#submit').fadeOut();
            $("#alert").fadeOut()
            $("#success").show().delay(2000).fadeOut();
            setTimeout('location.reload()', 3000);

        }
        else{
            $("#alert").html("<div class='alert alert-danger alert-dismissible'>" +
                " <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
                " <h4><i class='icon fa fa-ban'></i>"+ req['message'] +" </div>");
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    }

    function saveForm(){
        var valid = formIsValid();
        if (valid == true){
             $("#alert").html("")
            postForm()
           }
        else{
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $("#alert").html("<div class='alert alert-danger alert-dismissible'>" +
                " <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " +
                "<h4><i class='icon fa fa-ban'></i> Ошибка!</h4> Заполните все поля помеченные красным! </div>")}
    }

    function formIsValid(){
        var brand_id = getcurrentBrandsid();
        var model_id = getcurrentModelsid();
        var submodel_id = getcurrentSubModelsId();
        var modify_id = getcurrentModifyId()
        var part_code = $('#part_code').val();
        var part_type = $('#part_type').val();
        var region =  $('#region').val();
        var customer_name = $('#customer_name').val()
        var city = $('#city').val();
        var tel = $('#phone').val();
        var email = $('#inputEmail3').val();
        var desc = $('#desc').val()
        var valid = true;

        //марка
        if (brand_id == 0){
            valid = false;
            $('#brands_label').addClass('text-red')
        }
        else{
            $('#brands_label').removeClass('text-red')
        }

        //модель
        if (model_id == 0){
            valid = false;
            $('#models_label').addClass('text-red')
        }
        else{
            $('#models_label').removeClass('text-red')
        }

        //тип агрегата
        if (part_type == ''){
             valid = false;
            $('#part_type_label').addClass('text-red')
        }
        else{
            $('#part_type_label').removeClass('text-red')
        }

        //регион
        if (region == 0){
            valid = false;
            $('#region_label').addClass('text-red')
        }
        else{
            $('#region_label').removeClass('text-red')
        }

        //телефон
         if (tel == ''){
             valid = false;
            $('#phone_label').addClass('text-red')
        }
        else{
            $('#phone_label').removeClass('text-red')
        }

        //клиент
         if (customer_name == ''){
             valid = false;
            $('#customer_name_label').addClass('text-red')
        }
        else{
            $('#customer_name_label').removeClass('text-red')
        }

        formData = {
            'brand_id': brand_id,
            'model_id': model_id,
            'submodel_id': submodel_id,
            'modify_id':modify_id,
            'part_code': part_code,
            'part_type': part_type,
            'region': region,
            'city': city,
            'customer_name' : customer_name,
            'tel': tel,
            'email': email,
            'desc': desc,
            'valid': valid,
            'csrfmiddlewaretoken': csrftoken
        };

        return valid

        }

    function showAlert(html){

    }
});