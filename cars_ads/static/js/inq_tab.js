$(document).ready(function () {
    var brandslist = [];
    var regionslist = [];
    var part_type = '';
    var favorit = $('#favorit').prop('checked');


    $(".brand_selector").select2();
    $(".region_selector").select2();

    $('body').on('change', '#br_select', function () {
       brand_filter_param();
    });

    $('body').on('change', '#rg_select', function () {
       region_filter_param();
    });

    $('body').on('change', '#pt_filter', function () {
       part_type_param();
    });

    $('body').on('change', '#favorit', function () {
       favorits_param();
    });


    // загружаем данные для первой страницы
    getInqtable('');

    //пометка избранного
    $('body').on('click', '.favor', function () {
        check_favorite(this);
    });

    // показать popup с деталями заявления
    $('body').on('click', '.popdown', function () {
        showInq(this);

    });


    function PaginatorGo(elem){
        link = '?'+elem.id;
        getInqtable(link)
    }

    function getInqtable(link) {

        if(link==''){
            url = "/inq/"
        }
        else{
            url = '/inq/'+link
        }

        $.ajax({
            type: "GET",
            url: url,
            data: {'brands': brandslist,
                    'regions': regionslist,
                    'part_type': part_type,
                    'favorit': favorit},
            success: function (data) {
                updateTable(data)
            },
            fail: function () {

            }

        });
    }

    function updateTable(data){
        req = JSON.parse(data);
        $('#inq_table').html(req['inq_table']);
        $('#inq_paginator').html(req['inq_paginator']);

        // реакция на клик в пагинаторе
        $('.paginator').click( function(){
            PaginatorGo(this); return false;
        });


         // подключаем popup
        $('.popdown').popdown();
    }

    function brand_filter_param(){
        brandslist = [];
        var selectedBrands = $('#br_filter .select2-selection__choice');
        for (var i = 0; i < selectedBrands.length; i++) {
            brandslist.push(selectedBrands[i].title)
        }
        getInqtable('');
    }

    function region_filter_param(){
        regionslist = [];
        var selectedRegions = $('#rg_filter .select2-selection__choice');
        for (var i = 0; i < selectedRegions.length; i++) {
            regionslist.push(selectedRegions[i].title)
        }
        getInqtable('');
    }

    function part_type_param(){
        part_type = $('#part_type').val();
        getInqtable('')
    }

    function favorits_param(){
        favorit = $('#favorit').prop('checked');
        getInqtable('')
    }

    function showInq(elem){
        // иначе попробуем получить popup с объявлением
        var id_elem = $(elem).closest('tr').attr('id');
        $(elem).closest('tr').attr('class', 'inq read');// для подсветки прочитанных
        $.fn.show_popdown("/get_inq/"+id_elem, {'width':'600px'});
    }

    function check_favorite(elem){
        var id = $(elem).closest('tr').attr('id');

        $.ajax({
            type: "GET",
            url: /check_favor/+id,
            data: {
            },
            success: function (data) {
                req = JSON.parse(data);
                changeFavorite(elem, req['action']);
            },
            fail: function () {

            }

        });
    }

    function changeFavorite(elem, action){
        if (action == 'add'){
            $(elem).attr('class','favor text-yellow glyphicon glyphicon-star');
        }
        else if (action =='del') {
            $(elem).attr('class','unfavor glyphicon glyphicon-star-empty');
        }
    }

    var csrftoken = getCookie('csrftoken');

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
}



});